FROM ubuntu

RUN apt update
RUN apt install --yes python3 python3-pip

WORKDIR /opt/api

RUN pip install "django == 3.2"
RUN pip install Pillow
RUN pip install psycopg2-binary
RUN pip install djangorestframework

COPY . ./

ENTRYPOINT ["/bin/sh", "entrypoint.sh"]
