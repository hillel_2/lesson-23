from dataclasses import dataclass


@dataclass
class Wall:
    name: str
    id: int


data = {
    'name': 'Name',
    'id': 'name'
}

wall = Wall(**data)
print(wall)


class A:
    def first(self):
        pass

class B(A):
    def second(self):
        pass


print(dir(B()))