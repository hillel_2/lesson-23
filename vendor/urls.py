from django.urls import path
from rest_framework.routers import DefaultRouter

from vendor.views import get_wallpapers, WallpaperItemsView, WallpaperItemView, WallpaperListView, WallpaperModelViewSet

router = DefaultRouter()
router.register('set', WallpaperModelViewSet)

urlpatterns = [
    path('all', get_wallpapers),
    path('view', WallpaperItemsView.as_view()),
    path('view/<int:pk>', WallpaperItemView.as_view()),
    path('gen_view', WallpaperListView.as_view()),
    *router.urls
]
