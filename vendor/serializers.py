from django.contrib.auth.models import User
from rest_framework import serializers

from vendor.models import Wallpaper, Brand, Tags


class WallpaperSerializer(serializers.ModelSerializer):

    #brand = serializers.SlugRelatedField(queryset=Brand.objects.all(), slug_field='name')
    tags = serializers.SlugRelatedField(queryset=Tags.objects.all(), slug_field='name', many=True)
    creator = serializers.SlugRelatedField(queryset=User.objects.all(), slug_field='username')

    class Meta:
        model = Wallpaper
        fields = ('id', 'name', 'brand', 'tags', 'color', 'price', 'available', 'quantity', 'description', 'cover', 'creator')
