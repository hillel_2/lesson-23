from django.contrib import admin

# Register your models here.
from vendor.models import Brand, Tags, Wallpaper

admin.site.register(Brand)
admin.site.register(Tags)
admin.site.register(Wallpaper)
