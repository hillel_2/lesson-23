from django.db import models
from django.contrib.auth.models import User
from django.db.models.functions import Lower


class Brand(models.Model):
    name = models.CharField(max_length=50)
    rating = models.FloatField(max_length=5)

    def __str__(self):
        return f'{self.pk}-{self.name}({self.rating})'


class Tags(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return str(self.name)


class Wallpaper(models.Model):
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    color = models.CharField(max_length=50)
    price = models.FloatField(null=True, blank=True)
    available = models.BooleanField(default=False)
    quantity = models.IntegerField(default=0)
    description = models.TextField()
    cover = models.ImageField(blank=True, null=True, default='placeholder.png')
    tags = models.ManyToManyField(Tags)
    creator = models.ForeignKey(User, null=True, blank=True, default=None, on_delete=models.SET_NULL)
    create_at = models.DateField(auto_now_add=True)
    update_at = models.DateField(auto_now=True)

    class Meta:
        ordering = [Lower('name')]

    def __str__(self):
        return f'{self.pk} - {self.brand} - {self.name}'

