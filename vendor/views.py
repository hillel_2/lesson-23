from django.shortcuts import get_object_or_404
from rest_framework.decorators import api_view
from rest_framework.generics import ListCreateAPIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework.views import APIView

from vendor.models import Wallpaper
from vendor.serializers import WallpaperSerializer


@api_view(['GET', 'POST'])
def get_wallpapers(request):
    if request.method == 'GET':
        wallpapers = Wallpaper.objects.all()
        response = WallpaperSerializer(wallpapers, many=True)
        return Response({'data': response.data})
    if request.method == 'POST':
        return Response({'message': 'created'})


class WallpaperItemsView(APIView):
    def get(self, *args, **kwargs):
        wallpapers = Wallpaper.objects.all()
        response = WallpaperSerializer(wallpapers, many=True)
        return Response({'data': response.data})

    def post(self, request):
        wallpaper = request.data.get('wallpaper')
        serialized = WallpaperSerializer(data=wallpaper)
        if serialized.is_valid(raise_exception=True):
            model = serialized.save()
            return Response({
                'status': 'ok',
                'message': f'Wallpaper {model.name} created successfully. '
            })


class WallpaperItemView(APIView):
    def get(self, request, pk):
        data = get_object_or_404(Wallpaper, pk=pk)
        response = WallpaperSerializer(data)
        return Response({'data': response.data})

    def put(self, request, pk):
        data = get_object_or_404(Wallpaper, pk=pk)
        wallpaper = request.data.get('wallpaper')
        serialized = WallpaperSerializer(instance=data, data=wallpaper, partial=True)
        if serialized.is_valid(raise_exception=True):
            model = serialized.save()
            return Response({
                'status': 'ok',
                'message': f'Wallpaper {model.name} updated successfully. '
            })


class FakeListModelMixin:
    """
    List a queryset.
    """
    def list(self, request, *args, **kwargs):
        data = super().list(request, *args, **kwargs)
        return Response({'new_data': 'hah your data have been stolen. Transfer money to my any cash wallet:',
                         'data': data.data})


class WallpaperListView(ListCreateAPIView):
    queryset = Wallpaper.objects.all()
    serializer_class = WallpaperSerializer


class WallpaperModelViewSet(ModelViewSet):
    queryset = Wallpaper.objects.all()
    serializer_class = WallpaperSerializer

    def perform_create(self, serializer):
        user = self.request.user
        serializer.save(creator=user)

